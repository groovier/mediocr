﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediOCR
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputFileName = args[0];
            string outputFileName = Path.GetFileNameWithoutExtension(inputFileName) + ".txt";
            IronOcr.License.LicenseKey = "IRONOCR-1129892AA3-159179-5D71EC-C519783B95-E97FBD6-UExA4B6AF01CF087D8-COMMUNITY.TRIAL.EXPIRES.15.DEC.2019";
            var Ocr = new IronOcr.AdvancedOcr()
            {
                EnhanceContrast = true,
                EnhanceResolution = true,
                Language = IronOcr.Languages.Danish.OcrLanguagePack,
                Strategy = IronOcr.AdvancedOcr.OcrStrategy.Advanced,
                InputImageType = IronOcr.AdvancedOcr.InputTypes.AutoDetect,
                RotateAndStraighten = true,
            };
            using (StreamWriter sw = File.CreateText(outputFileName))
            {
                var startTime = DateTime.Now;
                Console.WriteLine($"OCR scan started at {startTime}.");
                var Results = Ocr.Read(inputFileName);
                var endTime = DateTime.Now;
                Console.WriteLine($"OCR scan ended at {endTime}.");
                Console.WriteLine($"OCR scan time: {endTime - startTime}.");
                // Console.WriteLine(Results.Text);
                sw.Write(Results.Text);
            }
            Console.ReadLine();
        }
    }
}
